package net.minecraft.server;

public class ItemPiston extends ItemBlock {

    public ItemPiston(int i) {
        super(i);
        this.a(true);
    }
    
    public int getDropData(int i) {
        return i > 5 ? i : 0; // tssge preserve all side piston face pistons
    }

    public int filterData(int i) {
        return i > 5 ? i : 7; // tssge preserve all side piston face pistons
    }
}